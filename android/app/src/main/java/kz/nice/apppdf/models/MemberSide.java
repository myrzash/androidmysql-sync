package kz.nice.apppdf.models;

import java.io.Serializable;

import kz.nice.apppdf.extra.Language;

public class MemberSide implements Serializable {

    private int id;
    private String name_ru;
    private String name_en;

    public MemberSide() {
    }

    public MemberSide(int id, String name_ru, String name_en) {
        this.id = id;
        this.name_ru = name_ru;
        this.name_en = name_en;
    }

    public int getId() {
        return id;
    }

    public String getName_ru() {
        return name_ru;
    }

    public String getName_en() {
        return name_en;
    }
}
