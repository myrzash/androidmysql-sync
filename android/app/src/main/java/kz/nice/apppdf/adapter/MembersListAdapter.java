//package kz.nice.apppdf.adapter;
//
//import android.content.Context;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.BaseAdapter;
//import android.widget.ImageView;
//import android.widget.TextView;
//
//import java.util.ArrayList;
//
//import kz.nice.apppdf.R;
//import kz.nice.apppdf.models.Member;
//
//public class MembersListAdapter extends BaseAdapter  {
//
//    private final Context context;
//    private ArrayList<Member> list = null;
//
//    public MembersListAdapter(Context context, ArrayList<Member> members) {
//        this.context = context;
//        this.list = members;
//    }
//
//    @Override
//    public int getCount() {
//        return list.size();
//    }
//
//    @Override
//    public Member getItem(int i) {
//        return list.get(i);
//    }
//
//    @Override
//    public long getItemId(int i) {
//        return i;
//    }
//
//    @Override
//    public View getView(int i, View view, ViewGroup viewGroup) {
//
//        if (view == null) {
//            view = LayoutInflater.from(this.context).inflate(R.layout.item_list_members, viewGroup, false);
//        }
//
//        Member member = getItem(i);
//        TextView tvName = view.findViewById(R.id.textItemMemberName);
//        tvName.setText(member.getName());
//        TextView tvPost = view.findViewById(R.id.textItemMemberPost);
//        tvPost.setText(member.getPosition());
//        ImageView imagePhoto = view.findViewById(R.id.imageItemMemberPhoto);
//        imagePhoto.setImageResource(R.drawable.member_test);
//        return view;
//    }
//}
