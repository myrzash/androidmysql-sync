package kz.nice.apppdf.extra;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import kz.nice.apppdf.Splash;
import kz.nice.apppdf.connections.Configuration;

public class ImageHelper {

    private final Context context;
    public static final String FOLDER_ROOT = "/sii/";
    public static final String FOLDER_BACKGROUNDS = "/sii/backgrounds/";
    public static final String FOLDER_MEMBERS = "/sii/members/";
    public static final String FOLDER_FILES = "/sii/files/";

    public ImageHelper(Context context) {
        this.context = context;
    }

    public boolean downloadPdfFile(String urlString, String toDirectory, String fileName) {

        try {
            InputStream input = null;
            File folder = new File(Environment.getExternalStorageDirectory(), toDirectory);
            if (!folder.exists()) {
                folder.mkdirs();
            }

            try {
                URL url = new URL(urlString + fileName); // link of the song which you want to download like (http://...)
                input = url.openStream();
                String path = Environment.getExternalStorageDirectory().toString() + toDirectory;
                OutputStream output = new FileOutputStream(new File(path, fileName));
                try {
                    byte[] buffer = new byte[1024];
                    int bytesRead = 0;
                    while ((bytesRead = input.read(buffer, 0, buffer.length)) >= 0) {
                        output.write(buffer, 0, bytesRead);
                    }
                } catch (Exception exception) {
                    return false;
                }
                output.close();
            } catch (Exception exception) {
                return false;
            } finally {
                input.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        //        try {
//            File folder = new File(Environment.getExternalStorage(), toDirectory);
//            if (!folder.exists()) {
//                folder.mkdirs();
//            }
//            String path = Environment.getExternalStorage().toString() + toDirectory;
//            Log.d(Splash.TAG, "path: " + path);
//
//            File file = new File(path, fileName);
//            FileOutputStream out = new FileOutputStream(file);
//            OutputStreamWriter myOutWriter = new OutputStreamWriter(out);
//            String data = getDataFile(url, fileName);
//            if (data == null || data.length() == 0) return false;
//            myOutWriter.append(data);
//            myOutWriter.close();
//            out.flush();
//            out.close();
//
//        }

        return true;
    }

    private String getDataFile(String urlString, String fileName) {
        StringBuilder builder = new StringBuilder();
        try {
            URL url = new URL(urlString + fileName);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            if (urlConnection.getResponseCode() == 200) {
                InputStream inputStream = new BufferedInputStream(urlConnection.getInputStream());

                BufferedReader br = new BufferedReader(
                        new InputStreamReader(inputStream, "UTF-8")); //new BufferedReader(new FileReader(file));
                String line = null;
                while ((line = br.readLine()) != null) {
                    builder.append(line);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return builder.toString();
    }

//    private void readFile(String filePath) {
//        File sdcard = Environment.getExternalStorage();
//        try {
//            BufferedReader br = new BufferedReader(
//                    new InputStreamReader(getAssets().open(filePath), "UTF-8")); //new BufferedReader(new FileReader(file));
//            String line = null;
//            List<String> lines = new ArrayList<String>();
//
//            while ((line = br.readLine()) != null) {
//                lines.add(line);
//                tvReader.append(line);
//                tvReader.append("\n");
//            }
//            String[] arr = lines.toArray(new String[lines.size()]);
//            Log.d("FILE", "arr: " + arr);
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }


    public boolean downloadPhoto(String url, String toDirectory, String fileName) {
        try {
            File folder = new File(Environment.getExternalStorageDirectory(), toDirectory);
            if (!folder.exists()) {
                folder.mkdirs();
            }
            Log.d("LOG_TAG", "context.getExternalStorage() = " + Environment.getExternalStorageDirectory());
            String path = Environment.getExternalStorageDirectory().toString() + toDirectory;
            File file = new File(path, fileName); // the File to save , append increasing numeric counter to prevent files from getting overwritten.
            OutputStream fOut = new FileOutputStream(file);
            Log.d(Splash.TAG, "path: " + path);
            Bitmap pictureBitmap = this.getBitmapFromURL(url, fileName);
            pictureBitmap.compress(Bitmap.CompressFormat.PNG, 90, fOut); // saving the Bitmap to a file compressed as a JPEG with 85% compression rate
            fOut.flush(); // Not really required
            fOut.close(); // do not forget to close the stream
            MediaStore.Images.Media.insertImage(context.getContentResolver(), file.getAbsolutePath(), file.getName(), file.getName());
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public Bitmap getBitmapFromURL(String url, String fileName) {
        Bitmap bitmap = null;
        try {
            InputStream in = new java.net.URL(url + fileName).openStream();
            bitmap = BitmapFactory.decodeStream(in);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bitmap;
    }

    public Bitmap getBitmapFromDir(String dirName, String fileName) {
        Bitmap bitmap = null;

        String path = Environment.getExternalStorageDirectory().toString() + dirName;
        File file = new File(path, fileName); // the File to save , append increasing numeric counter to prevent files from getting overwritten.
        if (!file.exists()) return null;
        try {
            bitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bitmap;
    }
}
