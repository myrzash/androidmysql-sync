package kz.nice.apppdf;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import kz.nice.apppdf.adapter.MembersExpandableListAdapter;
import kz.nice.apppdf.connections.Configuration;
import kz.nice.apppdf.connections.HttpHandler;
import kz.nice.apppdf.extra.Language;
import kz.nice.apppdf.models.Member;
import kz.nice.apppdf.models.MemberSide;

public class ActivityMembers extends LiveActivity {

    private ArrayList<Member> list;
    private MembersExpandableListAdapter adapter;
    private ExpandableListView expandableListView;
    private Language language;
    private MemberSide memberSide;
//    private ProgressBar progressBarLoadMembers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        language = new Language(this);
        language.setLang();

        setContentView(R.layout.activity_members);
        if (Splash.BACKGROUNDS != null && Splash.BACKGROUNDS.get(2) != null) {
            ImageView imageViewBG = findViewById(R.id.imageViewBG);
            imageViewBG.setImageBitmap(Splash.BACKGROUNDS.get(2));
        }

        memberSide = (MemberSide) getIntent().getSerializableExtra("member_side");
        Log.d("TAGS", "memberSide: " + memberSide);
        if (memberSide == null) {
            return;
        }
        setCustomToolbar(language.isRu() ? memberSide.getName_ru() : memberSide.getName_en());

        expandableListView = findViewById(R.id.listViewMembers);
        expandableListView.setGroupIndicator(null);
        expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                Member item = list.get(groupPosition);
                if (item.getStatus()) return false;
                return true;
            }
        });
        expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            public int previousGroup = -1;

            @Override
            public void onGroupExpand(int groupPosition) {
                if (groupPosition != previousGroup)
                    expandableListView.collapseGroup(previousGroup);
                previousGroup = groupPosition;
            }
        });

//        progressBarLoadMembers = findViewById(R.id.progressBarLoadMembers);
        list = Splash.getMembers(memberSide);
        if (list != null) {
            // progressBarLoadMembers.setVisibility(View.INVISIBLE);
            adapter = new MembersExpandableListAdapter(getApplicationContext(), list, language.isRu());
            expandableListView.setAdapter(adapter);
        }
    }

    private void setCustomToolbar(String title) {
        if (title != null) ((TextView) findViewById(R.id.title_page)).setText(title);
        findViewById(R.id.buttonBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}