package kz.nice.apppdf;

import android.content.Intent;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import kz.nice.apppdf.components.StyledButtonBold;
import kz.nice.apppdf.extra.Language;
import kz.nice.apppdf.models.Menu;

public class MenuActivity extends LiveActivity {

    private LinearLayout linearLayoutButtons;
    private Language language;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        language = new Language(this);
        language.setLang();

        setContentView(R.layout.activity_menu);
        Menu menu = (Menu) getIntent().getSerializableExtra("current_menu");
        if (menu == null) {
            return;
        }

        if (Splash.BACKGROUNDS != null && Splash.BACKGROUNDS.get(2) != null) {
            ImageView imageViewBG = findViewById(R.id.imageViewBG);
            imageViewBG.setImageBitmap(Splash.BACKGROUNDS.get(2));
        }

        setCustomToolbar(language.isRu() ? menu.getName_ru() : menu.getName_en());
        linearLayoutButtons = findViewById(R.id.linLayButtons);
        changeList(menu.getId());
    }

    private void setCustomToolbar(String title) {
        if (title != null) ((TextView) findViewById(R.id.title_page)).setText(title);
        findViewById(R.id.buttonBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private ArrayList<Menu> getList(int subId) {
        ArrayList<Menu> list = new ArrayList<Menu>();
        for (Menu item : Main.primaryList) {
            if (item.getSub_id() == subId) list.add(item);
        }

        Collections.sort(list, new Comparator<Menu>() {
            @Override
            public int compare(Menu o1, Menu o2) {
                return o1.getSort() > o2.getSort() ? 1 : o1.getSort() < o2.getSort() ? -1 : 0;
            }
        });
        return list;
    }

    private void changeList(int subId) {
        ArrayList<Menu> list = getList(subId);
        linearLayoutButtons.removeAllViews();
        for (int i = 0; i < list.size(); i++) {
            Menu item = list.get(i);
            StyledButtonBold button = (StyledButtonBold) getLayoutInflater().inflate(R.layout.menu_button, null);
            button.setText(language.isRu() ? item.getName_ru() : item.getName_en());
            button.setTextColor(Splash.BUTTON_STYLE.getText_color());
            button.setLayoutParams(Main.layoutParams);
            button.setTag(item);
            String file = language.isRu() ? item.getFile_ru() : item.getFile_en();
            if (file == null || file.equals("")) {
                button.setOnClickListener(openSubMenuClickListener);
            } else {
                button.setOnClickListener(openPdfClickListener);
            }
            linearLayoutButtons.addView(button);
        }
    }

    private View.OnClickListener openSubMenuClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Menu menu = (Menu) v.getTag();
            startActivity(getIntent().putExtra("current_menu", menu));
            overridePendingTransition(0, 0);
        }
    };

    private View.OnClickListener openPdfClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Menu menu = (Menu) v.getTag();
            Intent i = new Intent(MenuActivity.this, ActivityPdfViewer.class);
            i.putExtra("menu", menu);
            startActivity(i);
            overridePendingTransition(0, 0);
        }
    };
}
