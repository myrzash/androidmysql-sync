package kz.nice.apppdf;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import kz.nice.apppdf.components.StyledButtonBold;
import kz.nice.apppdf.connections.Configuration;
import kz.nice.apppdf.connections.HttpHandler;
import kz.nice.apppdf.extra.Language;
import kz.nice.apppdf.models.Member;
import kz.nice.apppdf.models.MemberSide;

public class ActivityMemberSides extends LiveActivity {

    private List<MemberSide> list = null;
    private LinearLayout linearLayoutButtons;
    private Language language;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        language = new Language(this);
        language.setLang();
        setContentView(R.layout.activity_member_sides);
        if (Splash.BACKGROUNDS != null && Splash.BACKGROUNDS.get(2) != null) {
            ImageView imageViewBG = findViewById(R.id.imageViewBG);
            imageViewBG.setImageBitmap(Splash.BACKGROUNDS.get(2));
        }

        linearLayoutButtons = findViewById(R.id.linLayButtonsMembersSide);
        setCustomToolbar(null);
        list = Splash.MEMBER_SIDE;
        populateList();
    }

    private void setCustomToolbar(String title) {
        if (title != null) ((TextView) findViewById(R.id.title_page)).setText(title);
        findViewById(R.id.buttonBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void populateList() {
        linearLayoutButtons.removeAllViews();
        for (int i = 0; i < list.size(); i++) {
            final MemberSide memberSide = list.get(i);
            StyledButtonBold button = (StyledButtonBold) getLayoutInflater().inflate(R.layout.menu_button, null);
            button.setLayoutParams(Main.layoutParams);
            button.setText(language.isRu() ? memberSide.getName_ru() : memberSide.getName_en());
            button.setTextColor(Splash.BUTTON_STYLE.getText_color());
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getApplicationContext(), ActivityMembers.class);
                    intent.putExtra("member_side", memberSide);
                    startActivity(intent);
                    overridePendingTransition(0, 0);
                }
            });
            linearLayoutButtons.addView(button);
        }
    }
}
