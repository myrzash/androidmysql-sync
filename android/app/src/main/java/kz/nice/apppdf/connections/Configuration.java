package kz.nice.apppdf.connections;

public class Configuration {

    // TODO: Таблицы
    // Таблицы Фоны
    public static final String URL_TABLE_BACKGROUNDS = "https://app.fic.kz/api/get_backgrounds.php";

    // Таблица Стилей кнопок
    public static final String URL_TABLE_BUTTON_STYLES = "https://app.fic.kz/api/get_button_styles.php";

    // Таблица Стартовой страницы
    public static final String URL_TABLE_MAIN_PAGE = "https://app.fic.kz/api/get_main.php";

    // Таблица Участников
    public static final String URL_TABLE_MEMBERS = "https://app.fic.kz/api/get_members.php";

    // Таблица Сторон участников
    public static final String URL_TABLE_MEMBER_SIDE = "https://app.fic.kz/api/get_member_side.php";

    // Таблица Разделов меню
    public static final String URL_TABLE_MENU = "https://app.fic.kz/api/get_menu.php";

    //    TODO: ФАЙЛЫ
    //    где участники
    public static final String URL_DIR_PHOTOS = "https://app.fic.kz/images/participants/";
    //    таблица "menu" где pdf
    public static final String URL_DIR_PDF = "https://app.fic.kz/files/";
    //    где фоны
    public static final String URL_DIR_BACKGROUND = "https://app.fic.kz/images/background/";
    //    таблица "main"  (где лого будет)
    public static final String URL_DIR_LOGO = "https://app.fic.kz/images/main/";
}








