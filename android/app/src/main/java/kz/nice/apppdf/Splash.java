package kz.nice.apppdf;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import kz.nice.apppdf.connections.Configuration;
import kz.nice.apppdf.connections.HttpHandler;
import kz.nice.apppdf.extra.ImageHelper;
import kz.nice.apppdf.extra.Language;
import kz.nice.apppdf.models.ButtonStyle;
import kz.nice.apppdf.models.MainPage;
import kz.nice.apppdf.models.Member;
import kz.nice.apppdf.models.MemberSide;
import kz.nice.apppdf.models.Menu;

public class Splash extends LiveActivity {

    private ImageView imageViewSplash;
    public static final String TAG = "LOGS";
    public static Bitmap LOGO = null;
    public static ArrayList<Bitmap> BACKGROUNDS = null;
    public static List<MemberSide> MEMBER_SIDE = null;
    private static ArrayList<Member> MEMBERS = null;
    public static ArrayList<Menu> MENU = null;
    public static MainPage MAIN_PAGE = null;
    private static SharedPreferences sharedPreferences;
    private ProgressBar progressBarSplash;
    private ImageHelper imageHelper;
    public static ButtonStyle BUTTON_STYLE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        imageViewSplash = findViewById(R.id.imageViewSplash);
        progressBarSplash = findViewById(R.id.progressBarSplash);
        imageHelper = new ImageHelper(this);
        // new TaskLoadBackground().execute();
        new TaskGetFromInternet().execute();
    }

    private void startMainPage() {
        Intent i = new Intent(Splash.this, ActivitySelectLanguage.class);
        startActivity(i);
        finish();
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

//    private class TaskLoadBackground extends AsyncTask<Void, Void, Void> {
//
//        @Override
//        protected Void doInBackground(Void... voids) {
//            String jsonStr = sharedPreferences.getString("backgrounds", null);
//            if (jsonStr == null) return null;
//            try {
//                JSONObject jsonObj = new JSONObject(jsonStr);
//                JSONArray array = jsonObj.getJSONArray("backgrounds");
//                JSONObject c = array.getJSONObject(0);
//                Bitmap bitmap = imageHelper.getBitmapFromDir(ImageHelper.FOLDER_BACKGROUNDS, c.getString("photo1"));
//                if (bitmap != null) {
//                    imageViewSplash.setImageBitmap(bitmap);
//                }
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//            return null;
//        }
//    }

    private boolean saveToSharedPrefs(String key, String jsonString) {
        if (jsonString == null) return false;

        switch (key) {
            case "backgrounds":
                try {
                    JSONObject jsonObj = new JSONObject(jsonString);
                    JSONArray array = jsonObj.getJSONArray("backgrounds");
                    JSONObject c = array.getJSONObject(0);
                    for (int i = 1; i <= 3; i++) {
                        boolean downloaded = imageHelper.downloadPhoto(Configuration.URL_DIR_BACKGROUND, ImageHelper.FOLDER_BACKGROUNDS, c.getString("photo" + i));
                        Log.d(TAG, c.getString("photo" + i) + ", downloaded: " + downloaded);
                        if (!downloaded) return false;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    return false;
                }
                break;
            case "main_page":
                try {
                    JSONObject jsonObj = new JSONObject(jsonString);
                    JSONArray array = jsonObj.getJSONArray("main");
                    JSONObject c = array.getJSONObject(0);
                    boolean downloaded = imageHelper.downloadPhoto(Configuration.URL_DIR_LOGO, ImageHelper.FOLDER_ROOT, c.getString("photo_ru"));
                    Log.d(TAG, c.getString("photo_ru") + ", downloaded: " + downloaded);
                    if (!downloaded) return false;
                } catch (JSONException e) {
                    e.printStackTrace();
                    return false;
                }
                break;
            case "members":
                try {
                    JSONObject jsonObj = new JSONObject(jsonString);
                    JSONArray array = jsonObj.getJSONArray("members");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        boolean downloaded = imageHelper.downloadPhoto(Configuration.URL_DIR_PHOTOS, ImageHelper.FOLDER_MEMBERS, c.getString("photo"));
                        Log.d(TAG, c.getString("photo") + ", downloaded: " + downloaded);
                        if (!downloaded) return false;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    return false;
                }
                break;
            case "menu":
                try {
                    JSONObject jsonObj = new JSONObject(jsonString);
                    JSONArray array = jsonObj.getJSONArray("menu");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);

                        String file_ru = c.getString("file_ru");
                        if (file_ru.trim().length() != 0 && file_ru != null) {
                            boolean downloaded = imageHelper.downloadPdfFile(Configuration.URL_DIR_PDF, ImageHelper.FOLDER_FILES, file_ru);
                            Log.d(TAG, file_ru + ", downloaded: " + downloaded);
                            if (!downloaded) return false;
                        }

                        String file_en = c.getString("file_en");
                        if (file_en.trim().length() != 0 && file_en != null) {
                            boolean downloaded = imageHelper.downloadPdfFile(Configuration.URL_DIR_PDF, ImageHelper.FOLDER_FILES, file_en);
                            Log.d(TAG, file_en + ", downloaded: " + downloaded);
                            if (!downloaded) return false;
                        }

                    }
                } catch (final JSONException e) {
                    e.printStackTrace();
                    return false;
                }
                break;
            default:
                break;
        }

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, jsonString);
        editor.commit();
        return true;
    }

    private class TaskGetFromInternet extends AsyncTask<Void, Integer, Boolean> {

        String[][] downloads = new String[][]{
                {Configuration.URL_TABLE_BACKGROUNDS, "backgrounds"},// PHOTOS
                {Configuration.URL_TABLE_MAIN_PAGE, "main_page"},//LOGO
                {Configuration.URL_TABLE_MENU, "menu"},//PDF
                {Configuration.URL_TABLE_MEMBER_SIDE, "member_side"},
                {Configuration.URL_TABLE_MEMBERS, "members"},//PHOTOS
                {Configuration.URL_TABLE_BUTTON_STYLES, "button_styles"}//PHOTOS
        };

//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            progressBarSplash.setProgress(0);
//            progressBarSplash.setIndeterminate(false);
//            progressBarSplash.setMax(downloads.length);
//        }

        @Override
        protected Boolean doInBackground(Void... voids) {

            HttpHandler sh = new HttpHandler();
            for (int i = 0; i < downloads.length; i++) {
                String jsonStr = sh.makeServiceCall(downloads[i][0]);
                boolean loaded = saveToSharedPrefs(downloads[i][1], jsonStr);
                Log.i(TAG, "Response from url: " + downloads[i][1] + ": " + loaded);
                if (!loaded) return false;
                // publishProgress(i+1);
            }

            return true;
        }

        @Override
        protected void onPostExecute(Boolean downloaded) {
            super.onPostExecute(downloaded);
            new TaskGetLocal().execute();
        }

//        @Override
//        protected void onProgressUpdate(Integer... values) {
//            // txt.setText("Running..."+ values[0]);
//            progressBarSplash.setProgress(values[0]);
//        }
    }

    private class TaskGetLocal extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Void... voids) {

            BACKGROUNDS = getBackgrounds();
            if (BACKGROUNDS == null) return false;

            MEMBER_SIDE = getMemberSide();
            if (MEMBER_SIDE == null) return false;

            MAIN_PAGE = getMainPage();
            if (MAIN_PAGE == null) return false;

            MENU = getMenu();
            if (MENU == null) return false;

            MEMBERS = getMembers();
            if (MEMBERS == null) return false;

            BUTTON_STYLE = getButtonStyle();
            if (BUTTON_STYLE == null) return false;

            return true;
        }

        private ButtonStyle getButtonStyle() {
            String jsonStr = sharedPreferences.getString("button_styles", null);
            if (jsonStr == null) return null;
            ButtonStyle result = null;
            try {
                JSONObject jsonObj = new JSONObject(jsonStr);
                JSONArray array = jsonObj.getJSONArray("button_styles");

                for (int i = 0; i < array.length(); i++) {
                    JSONObject c = array.getJSONObject(i);
                    if (c.getInt("status") == 1) {
                        return new ButtonStyle(
                                c.getString("button_background_color"),
                                c.getString("text_color"),
                                c.getInt("opacity"),
                                c.getInt("corner_radius")
                        );
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return result;
        }

        private ArrayList<Bitmap> getBackgrounds() {
            String jsonStr = sharedPreferences.getString("backgrounds", null);
            if (jsonStr == null) return null;

            ArrayList<Bitmap> result = null;
            try {
                JSONObject jsonObj = new JSONObject(jsonStr);
                JSONArray array = jsonObj.getJSONArray("backgrounds");
                JSONObject c = array.getJSONObject(0);
                result = new ArrayList<Bitmap>();
                for (int i = 1; i <= 3; i++) {
                    Log.d(TAG, c.getString("photo" + i));
                    Bitmap bitmap = imageHelper.getBitmapFromDir(ImageHelper.FOLDER_BACKGROUNDS, c.getString("photo" + i));
                    if (bitmap == null) return null;
                    result.add(bitmap);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return result;
        }

        private List<MemberSide> getMemberSide() {
            String jsonStr = sharedPreferences.getString("member_side", null);
            if (jsonStr == null) return null;

            ArrayList<MemberSide> list = null;
            try {
                JSONObject jsonObj = new JSONObject(jsonStr);
                JSONArray array = jsonObj.getJSONArray("member_side");
                list = new ArrayList<MemberSide>();
                for (int i = 0; i < array.length(); i++) {
                    JSONObject c = array.getJSONObject(i);
                    int id = c.getInt("id");
                    String name_ru = c.getString("name_ru");
                    String name_en = c.getString("name_en");
                    list.add(new MemberSide(id, name_ru, name_en));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return list;
        }

        private MainPage getMainPage() {
            String jsonStr = sharedPreferences.getString("main_page", null);
            if (jsonStr == null) return null;

            MainPage result = null;
            try {
                JSONObject jsonObj = new JSONObject(jsonStr);
                JSONArray array = jsonObj.getJSONArray("main");
                JSONObject c = array.getJSONObject(0);
                String name_ru = c.getString("text_ru");
                String name_en = c.getString("text_en");
                String text1_ru = c.getString("text1_ru");
                String text1_en = c.getString("text1_en");
                String date_ru = c.getString("date_ru");
                String date_en = c.getString("date_en");
                String photo = c.getString("photo_ru");
                result = new MainPage(name_ru, name_en, date_ru, date_en, text1_ru, text1_en);
                LOGO = imageHelper.getBitmapFromDir(ImageHelper.FOLDER_ROOT, photo);
                if (LOGO == null) return null;
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return result;
        }

        private ArrayList<Menu> getMenu() {
            String jsonStr = sharedPreferences.getString("menu", null);
            if (jsonStr == null) return null;

            ArrayList<Menu> result = null;
            try {
                JSONObject jsonObj = new JSONObject(jsonStr);
                JSONArray array = jsonObj.getJSONArray("menu");
                result = new ArrayList<Menu>();
                for (int i = 0; i < array.length(); i++) {
                    JSONObject c = array.getJSONObject(i);
                    int id = c.getInt("id");
                    int sort = c.getInt("sort");
                    int sub_id = c.getInt("sub_id");
                    String name_ru = c.getString("name_ru");
                    String name_en = c.getString("name_en");
                    String file_ru = c.getString("file_ru");
                    String file_en = c.getString("file_en");
                    result.add(new Menu(id, sub_id, sort, name_ru, name_en, file_ru, file_en));
                }
            } catch (final JSONException e) {
                e.printStackTrace();
            }
            return result;
        }

        private ArrayList<Member> getMembers() {
            String jsonStr = sharedPreferences.getString("members", null);
            ArrayList<Member> result = null;
            try {
                JSONObject jsonObj = new JSONObject(jsonStr);
                JSONArray array = jsonObj.getJSONArray("members");
                result = new ArrayList<Member>();
                for (int i = 0; i < array.length(); i++) {
                    JSONObject c = array.getJSONObject(i);
                    int side_id = c.getInt("side_id");
                    String name_ru = c.getString("name_ru");
                    String name_en = c.getString("name_en");
                    String position_ru = c.getString("position_ru");
                    String position_en = c.getString("position_en");
                    String bio_ru = c.getString("bio_ru");
                    String bio_en = c.getString("bio_en");
                    int status = c.getInt("status");
                    int sort = c.getInt("sort");
                    Bitmap photo = imageHelper.getBitmapFromDir(ImageHelper.FOLDER_MEMBERS, c.getString("photo"));
                    if (photo == null) return null;
                    result.add(new Member(name_ru, name_en, photo, position_ru, position_en, bio_ru, bio_en, side_id, status, sort));
                }

                Collections.sort(result, new Comparator<Member>() {
                    @Override
                    public int compare(Member o1, Member o2) {
                        return o1.getSort() > o2.getSort() ? 1 : o1.getSort() < o2.getSort() ? -1 : 0;
                    }
                });

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return result;
        }

        @Override
        protected void onPostExecute(Boolean loaded) {
            super.onPostExecute(loaded);
            progressBarSplash.setVisibility(View.INVISIBLE);
            if (loaded) {
                Log.d(TAG, "LOADED SUCCESSFULL");
                startMainPage();
            } else {
                Toast.makeText(getApplicationContext(), R.string.check_internet_connection, Toast.LENGTH_SHORT).show();
            }
        }
    }

    public static ArrayList<Member> getMembers(MemberSide memberSide) {
        if (MEMBERS == null) return null;
        ArrayList<Member> result = new ArrayList<Member>();
        for (Member item : MEMBERS) {
            if (memberSide.getId() != item.getSide_id()) continue;
            result.add(item);
        }
        return result;
    }
}