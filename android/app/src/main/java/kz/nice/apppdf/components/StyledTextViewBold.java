package kz.nice.apppdf.components;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import kz.nice.apppdf.extra.FontFactory;

public class StyledTextViewBold extends TextView {

    public StyledTextViewBold(Context context) {
        super(context);
    }

    public StyledTextViewBold(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public StyledTextViewBold(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void setTypeface(Typeface tf) {
        super.setTypeface(FontFactory.getFontBold(getContext()));
    }
}
