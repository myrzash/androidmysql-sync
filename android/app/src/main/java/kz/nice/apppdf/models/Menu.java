package kz.nice.apppdf.models;

import java.io.Serializable;

public class Menu implements Serializable{
    private int id;
    private int sub_id;
    private int sort;
    private String name_ru;
    private String name_en;
    private String file_ru = null;
    private String file_en = null;

    public Menu() {
    }

    public Menu(int id, int sub_id, int sort, String name_ru, String name_en, String file_ru, String file_en) {
        this.id = id;
        this.sub_id = sub_id;
        this.sort = sort;
        this.name_ru = name_ru;
        this.name_en = name_en;
        this.file_ru = file_ru;
        this.file_en = file_en;
    }

    public int getId() {
        return id;
    }

    public int getSub_id() {
        return sub_id;
    }

    public int getSort() {
        return sort;
    }

    public String getName_ru() {
        return name_ru;
    }

    public String getName_en() {
        return name_en;
    }

    public String getFile_ru() {
        return file_ru;
    }

    public String getFile_en() {
        return file_en;
    }

}
