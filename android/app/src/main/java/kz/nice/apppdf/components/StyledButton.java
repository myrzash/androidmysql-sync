package kz.nice.apppdf.components;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

import kz.nice.apppdf.extra.FontFactory;

public class StyledButton extends Button {
//    private int[] padding = new int []{40,40,40,40};

    public StyledButton(Context context) {
        super(context);
    }

    public StyledButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public StyledButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void setTypeface(Typeface tf) {
        super.setTypeface(FontFactory.getFont(getContext()));
    }

//    @Override
//    public void setPadding(int left, int top, int right, int bottom) {
//        super.setPadding(padding[0], padding[1], padding[2], padding[3]);
//    }
}
