package kz.nice.apppdf.adapter;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;

import kz.nice.apppdf.R;
import kz.nice.apppdf.Splash;
import kz.nice.apppdf.models.Member;

public class MembersExpandableListAdapter extends BaseExpandableListAdapter {

    private final Context context;
    private final boolean isRu;
    private ArrayList<Member> list;

    public MembersExpandableListAdapter(Context context, ArrayList<Member> members, boolean isRu) {
        this.context = context;
        this.list = members;
        this.isRu = isRu;
    }

    @Override
    public int getGroupCount() {
        return list.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return 1;
    }

    @Override
    public Member getGroup(int groupPosition) {
        return list.get(groupPosition);
    }

    @Override
    public String getChild(int groupPosition, int childPosition) {
        return isRu ? getGroup(groupPosition).getBio_ru() : getGroup(groupPosition).getBio_en();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = LayoutInflater.from(this.context).inflate(R.layout.item_list_members, parent, false);
        }
        View bg = view.findViewById(R.id.itemListBG);
        bg.setBackgroundDrawable(Splash.BUTTON_STYLE.getRectShapeItemList());

        Member member = getGroup(groupPosition);
        TextView tvName = view.findViewById(R.id.textItemMemberName);
        tvName.setText(isRu ? member.getName_ru() : member.getName_en());
        TextView tvPost = view.findViewById(R.id.textItemMemberPost);
        tvPost.setText(isRu ? member.getPosition_ru() : member.getPosition_en());
        ImageView imagePhoto = view.findViewById(R.id.imageItemMemberPhoto);
        imagePhoto.setImageBitmap(member.getPhoto());
        return view;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = LayoutInflater.from(this.context).inflate(R.layout.item_list_members_info, parent, false);
        }
        View bg = view.findViewById(R.id.itemListBG);
        bg.setBackgroundDrawable(Splash.BUTTON_STYLE.getRectShapeItemList());

        TextView textItemMemberInfo = view.findViewById(R.id.textItemMemberInfo);
        textItemMemberInfo.setText(Html.fromHtml(getChild(groupPosition, childPosition)));
        return view;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }

}
