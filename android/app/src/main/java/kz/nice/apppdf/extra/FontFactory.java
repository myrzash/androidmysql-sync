package kz.nice.apppdf.extra;

import android.content.Context;
import android.graphics.Typeface;

public class FontFactory {

    private final static String FONT_1 = "GOTHAPROREG.OTF";
    private final static String FONT_BOLD = "GOTHAPROBOL.OTF";
//    private final static String FONT_1 = "Roboto-Light.ttf";
//    private final static String FONT_BOLD = "Roboto-Bold.ttf";
    private static Typeface font1 = null;
    private static Typeface fontBold = null;

    public static Typeface getFont(Context context) {
        if (font1 == null) {
            font1 = Typeface.createFromAsset(context.getAssets(), FONT_1);
        }
        return font1;
    }

    public static Typeface getFontBold(Context context) {
        if (fontBold == null) {
            fontBold = Typeface.createFromAsset(context.getAssets(), FONT_BOLD);
        }
        return fontBold;
    }
}
