package kz.nice.apppdf.extra;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.preference.PreferenceManager;

import java.util.Locale;

public class Language {
    private final SharedPreferences sharedPreferences;
    Activity context;
    private static final String language = null;

    public Language(Activity context) {
        this.context = context;
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public void setLanguage(String language) {
        if (language == null) return;
        try {
            String languageToLoad = language; // your language
            Locale locale = new Locale(languageToLoad);
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            context.getBaseContext().getResources().updateConfiguration(config,
                    context.getBaseContext().getResources().getDisplayMetrics());
        } catch (Exception e) {
        }
    }

    public void setLang() {
        String lang = sharedPreferences.getString("lang", language);
        setLanguage(lang);
    }

    public String getLang() {
        return sharedPreferences.getString("lang", language);
    }

    public boolean isRu(){
        if(this.getLang()==null) return false;
        return this.getLang().equals("ru");
    }
}

