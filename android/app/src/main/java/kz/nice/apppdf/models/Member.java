package kz.nice.apppdf.models;

import android.graphics.Bitmap;

public class Member {

    private String name_ru;
    private String name_en;
    private Bitmap photo;
    private String position_ru;
    private String position_en;
    private String bio_ru;
    private String bio_en;
    private int side_id;
    private int status;
    private int sort;

    public Member() {
    }

    public Member(String name_ru, String name_en, Bitmap photo, String position_ru, String position_en, String bio_ru, String bio_en, int side_id, int status, int sort) {
        this.name_ru = name_ru;
        this.name_en = name_en;
        this.photo = photo;
        this.position_ru = position_ru;
        this.position_en = position_en;
        this.bio_ru = bio_ru;
        this.bio_en = bio_en;
        this.side_id = side_id;
        this.status = status;
        this.sort = sort;
    }

    public String getName_ru() {
        return name_ru;
    }

    public String getName_en() {
        return name_en;
    }

    public Bitmap getPhoto() {
        return photo;
    }

    public String getPosition_ru() {
        return position_ru;
    }

    public String getPosition_en() {
        return position_en;
    }

    public String getBio_ru() {
        return bio_ru;
    }

    public String getBio_en() {
        return bio_en;
    }

    public int getSide_id() {
        return side_id;
    }

    public boolean getStatus (){
        return status!=0;
    }

    public int getSort() {
        return sort;
    }
}
