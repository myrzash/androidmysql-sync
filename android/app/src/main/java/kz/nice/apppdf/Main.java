package kz.nice.apppdf;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import kz.nice.apppdf.adapter.MembersExpandableListAdapter;
import kz.nice.apppdf.components.StyledButtonBold;
import kz.nice.apppdf.connections.Configuration;
import kz.nice.apppdf.connections.HttpHandler;
import kz.nice.apppdf.extra.Language;
import kz.nice.apppdf.models.Member;
import kz.nice.apppdf.models.Menu;

public class Main extends LiveActivity {

    private LinearLayout linearLayoutButtons;
    private ArrayList<Menu> list;
    public static ArrayList<Menu> primaryList;
    private static long back_pressed = 0;
    public static final LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
    private Language language;
    private ProgressBar progressBarMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        language = new Language(this);
        language.setLang();
        setContentView(R.layout.activity_main);

        if(Splash.LOGO!=null){
            ImageView imageViewLogog = findViewById(R.id.imageViewLogo);
            imageViewLogog.setImageBitmap(Splash.LOGO);
        }

        if (Splash.BACKGROUNDS != null && Splash.BACKGROUNDS.get(2) != null) {
            ImageView imageViewBG = findViewById(R.id.imageViewBG);
            imageViewBG.setImageBitmap(Splash.BACKGROUNDS.get(2));
        }

        linearLayoutButtons = findViewById(R.id.linLayButtons);
        layoutParams.setMargins(0, 10, 0, 10);
        progressBarMenu = findViewById(R.id.progressBarMenu);

        primaryList = Splash.MENU;
        if(primaryList!=null) {
            changeList(0);
        }
    }

    @Override
    public void onBackPressed() {

        if (back_pressed + 2000 > System.currentTimeMillis()) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            overridePendingTransition(android.R.anim.fade_in,
                    android.R.anim.fade_out);
        } else
            Toast.makeText(getApplicationContext(), R.string.toast_exit, Toast.LENGTH_SHORT).show();
        back_pressed = System.currentTimeMillis();

    }

    public void goToLanguages(View v) {
        startActivity(new Intent(getApplicationContext(), ActivitySelectLanguage.class));
        finish();
        overridePendingTransition(0, 0);
    }

//    private ArrayList<Menu> initList() {
//        ArrayList<Menu> list = new ArrayList<Menu>();
//        list.add(new Menu(1, 0, "Раздел 1", 1));
//        list.add(new Menu(2, 0, "Раздел 2", 2));
//        list.add(new Menu(3, 0, "Раздел 3", 3, "file"));
//        list.add(new Menu(4, 0, "Раздел 5", 5, "file"));
//        list.add(new Menu(5, 0, "Раздел 4", 4, "file"));
//        list.add(new Menu(6, 1, "Раздел 11", 10));
//        list.add(new Menu(7, 1, "Раздел 12", 20, "file"));
//        list.add(new Menu(8, 1, "Раздел 13", 30, "file"));
//        list.add(new Menu(9, 0, "Раздел 6", 6, "file"));
//        list.add(new Menu(10, 2, "Раздел 21", 10, "file"));
//        list.add(new Menu(11, 2, "Раздел 22", 20, "file"));
//        list.add(new Menu(12, 2, "Раздел 23", 30, "file"));
//        list.add(new Menu(13, 6, "Раздел 111", 10, "file"));
//        list.add(new Menu(14, 6, "Раздел 112", 20, "file"));
//        list.add(new Menu(15, 6, "Раздел 113", 30, "file"));
//        return list;
//    }

    private ArrayList<Menu> getList(int subId) {
        ArrayList<Menu> list = new ArrayList<Menu>();
        for (Menu item : primaryList) {
            if (item.getSub_id() == subId) list.add(item);
        }

        Collections.sort(list, new Comparator<Menu>() {
            @Override
            public int compare(Menu o1, Menu o2) {
                return o1.getSort() > o2.getSort() ? 1 : o1.getSort() < o2.getSort() ? -1 : 0;
            }
        });
        return list;
    }

    private void changeList(int subId) {
        list = getList(subId);
        linearLayoutButtons.removeAllViews();
        for (int i = 0; i < list.size(); i++) {
            Menu item = list.get(i);
            StyledButtonBold button = (StyledButtonBold) getLayoutInflater().inflate(R.layout.menu_button, null);
            button.setText(language.isRu() ? item.getName_ru() : item.getName_en());
            button.setTextColor(Splash.BUTTON_STYLE.getText_color());
            button.setLayoutParams(Main.layoutParams);
            button.setTag(item);
            String file = language.isRu() ? item.getFile_ru() : item.getFile_en();
            if (item.getId() == 1) {
                button.setOnClickListener(openMembersListener);
            } else if (file == null || file.equals("")) {
                button.setOnClickListener(openSubMenuClickListener);
            } else {
                button.setOnClickListener(openPdfClickListener);
            }
            linearLayoutButtons.addView(button);
        }
    }


    private View.OnClickListener openMembersListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            startActivity(new Intent(Main.this, ActivityMemberSides.class));
            overridePendingTransition(0, 0);
        }
    };


    private View.OnClickListener openSubMenuClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Menu menu = (Menu) v.getTag();
            startActivity(new Intent(Main.this, MenuActivity.class).putExtra("current_menu", menu));
            overridePendingTransition(0, 0);
        }
    };

    private View.OnClickListener openPdfClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Menu menu = (Menu) v.getTag();
            Intent i = new Intent(Main.this, ActivityPdfViewer.class);
            i.putExtra("menu", menu);
            startActivity(i);
            overridePendingTransition(0, 0);
        }
    };
}