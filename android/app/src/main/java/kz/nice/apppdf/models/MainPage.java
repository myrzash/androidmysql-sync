package kz.nice.apppdf.models;

public class MainPage {
    //    private String id;
    private String text_ru;
    private String text_en;
    private String text1_ru;
    private String text1_en;

    //    private String photo_ru;
//    private String photo_en;
    private String date_ru;
    private String date_en;

    public MainPage(String text_ru, String text_en, String date_ru, String date_en, String text1_ru, String text1_en) {
        this.text_ru = text_ru;
        this.text_en = text_en;
        this.text1_ru = text1_ru;
        this.text1_en = text1_en;
        this.date_ru = date_ru;
        this.date_en = date_en;
    }

    public String getText_ru() {
        return text_ru;
    }

    public String getText_en() {
        return text_en;
    }

    public String getDate_ru() {
        return date_ru;
    }

    public String getDate_en() {
        return date_en;
    }

    public String getText1_ru() {
        return text1_ru;
    }

    public String getText1_en() {
        return text1_en;
    }

}
