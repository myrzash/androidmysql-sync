package kz.nice.apppdf.models;

import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.StateListDrawable;
import android.graphics.drawable.shapes.RectShape;
import android.graphics.drawable.shapes.RoundRectShape;

public class ButtonStyle {

    private String button_background_color;
    private int text_color;
    private int corner_radius;
    private int opacity;

    public ButtonStyle(String button_background_color, String text_color, int opacity, int corner_radius) {
        this.button_background_color = button_background_color;
        text_color = text_color.equals("#fff") ? "#ffffff" : text_color;
        this.text_color = Color.parseColor(text_color);
        this.opacity = (opacity > 100 || opacity <= 0) ? 100 : opacity;
        this.corner_radius = corner_radius;
    }

    public int getText_color() {
        return text_color;
    }

    private GradientDrawable getRectShape() {
        GradientDrawable shape = new GradientDrawable();
        shape.setCornerRadius(corner_radius);
        return shape;
    }

    public GradientDrawable getRectShapeItemList() {
        GradientDrawable shape = new GradientDrawable();
        shape.setCornerRadius(30);
        shape.setColor(Color.parseColor(button_background_color));
        shape.setAlpha(160);
        return shape;
    }

    public StateListDrawable getBackground() {
        GradientDrawable shapePressed = this.getRectShape();
        shapePressed.setAlpha(5);
        shapePressed.setColor(Color.GRAY);

        GradientDrawable shape = this.getRectShape();
        shape.setColor(Color.parseColor(button_background_color));

        StateListDrawable res = new StateListDrawable();
//        res.setExitFadeDuration(300);
        res.setAlpha(this.opacity);
        res.addState(new int[]{android.R.attr.state_pressed}, shapePressed);
        res.addState(new int[]{}, shape);

        return res;
    }
}
