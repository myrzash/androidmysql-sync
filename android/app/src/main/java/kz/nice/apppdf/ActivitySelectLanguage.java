package kz.nice.apppdf;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.ArrayList;

import kz.nice.apppdf.components.StyledButton;
import kz.nice.apppdf.connections.Configuration;
import kz.nice.apppdf.connections.HttpHandler;
import kz.nice.apppdf.extra.FontFactory;
import kz.nice.apppdf.extra.Language;
import kz.nice.apppdf.models.MainPage;
import kz.nice.apppdf.models.MemberSide;

public class ActivitySelectLanguage extends LiveActivity {

    private Language language;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        language = new Language(this);
        if (language.getLang() == null) {
            language.setLanguage("en");
        } else {
            language.setLang();
        }

        setContentView(R.layout.activity_select_language);

        if (Splash.BACKGROUNDS != null && Splash.BACKGROUNDS.get(1) != null) {
            ImageView imageViewBG = findViewById(R.id.imageViewBGselectLanguage);
            imageViewBG.setImageBitmap(Splash.BACKGROUNDS.get(1));
        }

        int textColor = Splash.BUTTON_STYLE.getText_color();

        Button btnRU = findViewById(R.id.langRU);
        Button btnEN = findViewById(R.id.langEN);
        btnRU.setTextColor(textColor);
        btnEN.setTextColor(textColor);
        btnRU.setOnClickListener(listener);
        btnEN.setOnClickListener(listener);

        Typeface boldTypeface = FontFactory.getFontBold(this);
        Typeface normalTypeface = FontFactory.getFont(this);
        if (language.isRu()) {
            btnRU.setTypeface(boldTypeface);
            btnEN.setTypeface(normalTypeface);
        } else {
            btnEN.setTypeface(boldTypeface);
            btnRU.setTypeface(normalTypeface);
        }

        MainPage result = Splash.MAIN_PAGE;
        if (result != null) {
            TextView textViewTitleMain = findViewById(R.id.textViewTitleMain);
            textViewTitleMain.setTextColor(textColor);
            textViewTitleMain.setText(language.isRu() ? result.getText_ru() : result.getText_en());
            TextView textViewDate = findViewById(R.id.textViewDate);
            textViewDate.setTextColor(textColor);
            textViewDate.setText(language.isRu() ? result.getDate_ru() : result.getDate_en());

            TextView textViewText1 = findViewById(R.id.textViewText1);
            textViewText1.setTextColor(textColor);
            textViewText1.setText(language.isRu() ? result.getText1_ru() : result.getText1_en());
        }
        if (Splash.LOGO != null) {
            ImageView imageViewLogog = findViewById(R.id.imageViewLogo);
            imageViewLogog.setImageBitmap(Splash.LOGO);
        }
    }

    private View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            savePreferences(v.getTag().toString().toLowerCase().trim());
            finish();
            startActivity(new Intent(ActivitySelectLanguage.this, Main.class));
            overridePendingTransition(0, 0);
        }
    };

    private void savePreferences(String lang) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("lang", lang);
        editor.commit();
    }


}
