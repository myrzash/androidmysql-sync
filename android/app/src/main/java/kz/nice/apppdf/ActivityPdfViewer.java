package kz.nice.apppdf;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.listener.OnLoadCompleteListener;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import kz.nice.apppdf.LiveActivity;
import kz.nice.apppdf.R;
import kz.nice.apppdf.connections.Configuration;
import kz.nice.apppdf.extra.ImageHelper;
import kz.nice.apppdf.extra.Language;
import kz.nice.apppdf.models.MemberSide;
import kz.nice.apppdf.models.Menu;

public class ActivityPdfViewer extends LiveActivity {

    private PDFView pdfView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Language language = new Language(this);
        language.setLang();

        setContentView(R.layout.activity_pdf_viewer);

        Menu menu = (Menu) getIntent().getSerializableExtra("menu");

        if (menu == null) {
            return;
        }

        setCustomToolbar(language.isRu() ? menu.getName_ru() : menu.getName_en());

        if (Splash.BACKGROUNDS != null && Splash.BACKGROUNDS.get(2) != null) {
            ImageView imageViewBG = findViewById(R.id.imageViewBG);
            imageViewBG.setImageBitmap(Splash.BACKGROUNDS.get(2));
        }

        String fileName = language.isRu() ? menu.getFile_ru() : menu.getFile_en();
        if (fileName == null || fileName.trim().equals("")) return;

        pdfView = findViewById(R.id.pdfView);
        String path = Environment.getExternalStorageDirectory().toString() + ImageHelper.FOLDER_FILES;
        File file = new File(path, fileName);
        pdfView.fromFile(file)
                .enableSwipe(true)
                .enableAntialiasing(true)
                .enableDoubletap(true)
                .load();

//        RetrievePdfStream retrievePdfStream = new RetrievePdfStream();
//        retrievePdfStream.execute(fileName);

    }


    private void setCustomToolbar(String title) {
        if (title != null) ((TextView) findViewById(R.id.title_page)).setText(title);
        findViewById(R.id.buttonBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

/*    class RetrievePdfStream extends AsyncTask<String, Void, InputStream> {

        @Override
        protected InputStream doInBackground(String... strings) {
            InputStream inputStream = null;
            String fileName = strings[0];
            String path = Environment.getExternalStorageDirectory().toString() + ImageHelper.FOLDER_FILES;
            try {
                File file = new File(path, fileName);
                inputStream = new FileInputStream(file);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            return inputStream;
        }

        @Override
        protected void onPostExecute(InputStream inputStream) {
            if (inputStream != null) {
                pdfView.fromStream(inputStream)
                        .enableSwipe(true)
                        .enableAntialiasing(true)
                        .enableDoubletap(true)
                        .load();
            }

        }
    }*/
}
