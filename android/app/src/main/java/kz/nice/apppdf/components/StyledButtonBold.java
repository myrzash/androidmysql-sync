package kz.nice.apppdf.components;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.util.AttributeSet;
import android.widget.Button;

import kz.nice.apppdf.Splash;
import kz.nice.apppdf.extra.FontFactory;

public class StyledButtonBold extends Button {
//    private int[] padding = new int []{40,40,40,40};

    public StyledButtonBold(Context context) {
        super(context);
    }

    public StyledButtonBold(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public StyledButtonBold(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void setTypeface(Typeface tf) {
        super.setTypeface(FontFactory.getFontBold(getContext()));
    }

    @Override
    public void setBackground(Drawable background) {
        if (Splash.BUTTON_STYLE != null) {
            super.setBackground(Splash.BUTTON_STYLE.getBackground());
        } else {
            super.setBackground(background);
        }
    }
}
